#!/usr/bin/env bash
#**************************************************************************************************#
# build-scripts.sh                                                                                 #
#                                                                                                  #
# Deploy the latest changes from a Bitbucket repository to a WP Engine remote repository using the #
# Codeship CI/CD service.                                                                          #
#                                                                                                  #
# The project being deployed must contain a .env file with the following variables defined:        #
# DEV_REPO="git@git.wpengine.com:production/<repo-dev>.git"                                        #
# STAGING_REPO="git@git.wpengine.com:production/<repo-stg>.git"                                    #
# PRODUCTION_REPO="git@git.wpengine.com:production/<repo-prod>.git"                                #
#**************************************************************************************************#

# Import utility functions
source "$BUILD_SCRIPTS_DIR/utils.sh"

# The branch being deployed
BRANCH=$CI_BRANCH

# The directory where the the project being deployed be cloned
CLONE_DIR="$HOME/src/bitbucket.org/$CI_REPO_NAME"

# The committer name and email to use for the final WP Engine git commit
USER_EMAIL=$CI_COMMITTER_EMAIL
USER_NAME=$CI_COMMITTER_NAME

# The following environment variables must be present for the script to continue
REQUIRED_ENVIRONMENT_VARS=("CLONE_DIR" "BRANCH" "USER_EMAIL" "USER_NAME")
VALID_BRANCHES=("develop" "staging" "production")

# The directories to deploy
VALID_SYNC_TARGETS=("mu-plugins" "plugins" "themes")

# The path to the build artifact directory.
# The contents of the build artifact are what will ultimately be pushed to WP Engine.
ARTIFACT_DIR="$CLONE_DIR/../build_artifact"

# ------------------------------------------------------------------------------------------ #
# PrintEnvironmentSummary()                                                                  #
# Provide a summary of the key environment variable information at the start of the process. #
# ------------------------------------------------------------------------------------------ #
function PrintEnvironmentSummary() {
  cat << EOF

  Preparing to run the deployment process with the following settings:

    * Build artifact directory: $ARTIFACT_DIR
    * Deploy to branch: $BRANCH
    * CI Project clone directory: $CLONE_DIR
    * User email to use for deployment commit: $USER_EMAIL
    * User name to use for deployment commit: $USER_NAME
    * Commit message: "$CI_MESSAGE"

EOF
}

# --------------------------------------- #
# GetEnvFilePath()                        #
# Get the path to the project's .env file #
# --------------------------------------- #
function GetEnvFilePath() {
    echo ${CLONE_DIR}/.env
}

# --------------------------------------------------------------- #
# ValidateDotEnvFileExists()                                      #
# Ensure that the project has a .env file that can be read.       #
# Stops the current running process if no .env file can be found. #
# --------------------------------------------------------------- #
function ValidateDotEnvFileExists() {
  ENV_FILE_PATH=$(GetEnvFilePath)
  FileExists "$ENV_FILE_PATH"
  ENV_FILE_EXISTS_STATUS=$?
  if [[ "$ENV_FILE_EXISTS_STATUS" -gt 0 ]]; then
    PrintError "No .env file could be found at $ENV_FILE_PATH"
    exit 1
  fi

  PrintSuccess "Environment file found"
}

# Load the values from the project .env file.
# ------------------------------------------------------------------------------ #
# LoadDotEnvFile()                                                               #
# Load the environment variable data from the .env file into the current session #
# ------------------------------------------------------------------------------ #
function LoadDotEnvFile() {
    PrintHeader "Loading data from the project .env file"
    ENV_FILE_PATH="$(GetEnvFilePath)"
    PrintInfo "File location: $ENV_FILE_PATH"
    source "$ENV_FILE_PATH"
}

# -------------------------------------------------------------------------------- #
# ValidateEnvironmentVars()                                                        #
# Check to see if all required environment variables are present and have values.  #
# If any of the required environment variables are missing or empty, exit and stop #
# the current running process.                                                     #
# -------------------------------------------------------------------------------- #
function ValidateEnvironmentVars() {
    PrintHeader "Validating environment variables..."

    for var in "${REQUIRED_ENVIRONMENT_VARS[@]}"; do
      value=${!var}
      if [[ "$value" == "" ]]; then
        PrintError "Required environment variable $var is empty"
        exit 1
      fi
      PrintSuccess "Valid environment variable: $var:${ENDCOLOR} $value"
    done
}

# ------------------------------------------------------------------------------ #
# ValidateDeployBranch()                                                         #
# Make sure that current branch is in a list of valid deploy branches.           #
# If the branch is not a valid deploy branch, exit and stop the current process. #
# ------------------------------------------------------------------------------ #
function ValidateDeployBranch() {
    PrintHeader "Validating target deploy branch..."

    validBranchFound="false"
    for validBranch in "${VALID_BRANCHES[@]}"; do
      if [[ "$BRANCH" == "$validBranch" ]]; then
        PrintSuccess "Valid deploy branch: $BRANCH"
        validBranchFound="true"
      fi
    done

    if [[ "$validBranchFound" == "false" ]]; then
      PrintError "Branch [$BRANCH] is not a valid deploy branch."
      exit 1
    fi
}

# --------------------------------------------------------------------------- #
# GetRemoteRepoUrl()                                                          #
# Checks the current branch, and then looks for the {BRANCH}_REPO environment #
# variable that was loaded from the .env file.                                #
# If no repository URL can be loaded, exit and stop the current process.      #
# --------------------------------------------------------------------------- #
function GetRemoteRepoUrl() {
    REPO_KEY="$(ToUpperCase "$BRANCH")_REPO"
    REPO_URL="${!REPO_KEY}"
    if [[ "$REPO_URL" == "" ]]; then
      PrintError "Repository information not found in .env (${REPO_KEY}).  Invalid remote repo URL."
      exit 1
    fi

    echo "$REPO_URL";
}

# -------------------------------------------------------------------------------------------------- #
# IsValidTargetType()                                                                                #
# Parameter: $SYNC_TARGET The name of the directory to check against a list of valid sync targets.   #
# A safety check to make sure that the target directory is in a list of approved deployment targets. #
# If the target directory is not in the approved list, exit and stop the current process.            #
# -------------------------------------------------------------------------------------------------- #
function IsValidTargetType() {
    SYNC_TARGET=$1
    PrintHeader "Validating target \"$SYNC_TARGET\" against list of approved targets..."

    for validTarget in "${VALID_SYNC_TARGETS[@]}"; do
      if [[ "$SYNC_TARGET" == "$validTarget" ]]; then
        PrintSuccess "$SYNC_TARGET is a valid target."
        return 0
      fi
    done

    PrintError "${SYNC_TARGET} is not a valid target type"
    return 1
}

# ------------------------------------------------------------------------------------------ #
# PreflightChecks()                                                                          #
# Run through a set of pre-deploy validation actions to ensure that a deployment is allowed. #
# ------------------------------------------------------------------------------------------ #
function PreflightChecks() {
  PrintHeader "Running pre-flight validation checks"
  ValidateDotEnvFileExists
  LoadDotEnvFile
  ValidateEnvironmentVars
  ValidateDeployBranch
}

# ------------------------------------------------------------------------------------------ #
# RsyncDefaultArgs()                                                                         #
# Parameter: $SYNC_TARGET The name of the directory to build rsync arguments for.            #
# Echoes a string of arguments that can be used with the rsync process for a given directory #
# ------------------------------------------------------------------------------------------ #
function RsyncDefaultArgs() {
  SYNC_TARGET=$1
  if [[ "$SYNC_TARGET" == "" ]]; then
    PrintError "Sync target cannot be blank"
    exit 1
  fi

  SOURCE="${CLONE_DIR}/wp-content/${SYNC_TARGET}/"
  DESTINATION="${ARTIFACT_DIR}/wp-content/${SYNC_TARGET}"

  echo "$SOURCE $DESTINATION --checksum --delete --force --itemize-changes --prune-empty-dirs --archive --stats --exclude=node_modules"
}

# ------------------------------------------------------------------------------------------------------------------- #
# EnsureTargetDirectoryExists()                                                                                       #
# Parameter: $DIRECTORY The name of the directory to check for.
# Checks to see if a target directory exists, and creates it if it does not.                                          #
# This is necessary because rsync will fail if the source directory does not exist. If this is the first time a       #
# branch/repo is being committed to, the remote repo that is checked out will empty, which would cause rsync to fail. #
# ------------------------------------------------------------------------------------------------------------------- #
function EnsureTargetDirectoryExists() {
    DIRECTORY="$1"
    PrintHeader "Making sure that the $DIRECTORY directory exists"
    DirectoryExists "$DIRECTORY"
    TARGET_EXISTS_STATUS=$?
    if [[ "$TARGET_EXISTS_STATUS" -ne 0 ]]; then
      PrintWarning "Directory does not exist, creating it now."
      mkdir -p "$DIRECTORY"
    else
      PrintLine "$CHECKMARK Directory already exists, no action needed."
    fi

    return $TARGET_EXISTS_STATUS
}

# -------------------------------------------------------------------------------------------------------- #
# FoundChanges()                                                                                           #
# Parameter: $SYNC_TARGET The name of the directory to check for changes.
# Compare the contents of the WP Engine repository to the latest commit from Bitbucket.                    #
# Returns 1 if the target directory did not exist or rsync detected file additions, deletions, or changes. #
# Returns 0 if no file changes were detected.                                                              #
# -------------------------------------------------------------------------------------------------------- #
function FoundChanges() {
  SYNC_TARGET=$1

  IsValidTargetType "$SYNC_TARGET"
  IS_VALID_TARGET_TYPE_STATUS=$?
  if [[ "$IS_VALID_TARGET_TYPE_STATUS" -ne 0 ]]; then
    PrintLine "Invalid target type specified ($SYNC_TARGET). Aborting deploy."
    exit 1
  fi

  EnsureTargetDirectoryExists "$ARTIFACT_DIR/wp-content/$SYNC_TARGET"
  TARGET_DIR_ALREADY_EXISTED_STATUS=$?

  # If the target did not already exist, then whatever repo changes exist are new, so we will have changes to deploy.
  if [[ "$TARGET_DIR_ALREADY_EXISTED_STATUS" -ne 0 ]]; then
    return 1
  fi

  RSYNC_ARGS=$(RsyncDefaultArgs "$SYNC_TARGET")
  RSYNC_ARGS="$RSYNC_ARGS --dry-run"

  PrintLine "Doing a dry run rsync operation to check for file changes"
  PrintLine "rsync $RSYNC_ARGS"

  RSYNC_OUTPUT=$(rsync $RSYNC_ARGS)
  RSYNC_EXIT_STATUS=$?
  if [[ "$RSYNC_EXIT_STATUS" -ne 0 ]]; then
    exit 1;
  fi

  NUMBER_OF_FILES_CREATED_STRING=$(echo "$RSYNC_OUTPUT" | grep -E "Number of created files: ([0-9]+)")
  NUMBER_OF_FILES_CREATED_NUMBER=$(GetFirstNumberFromString "$NUMBER_OF_FILES_CREATED_STRING")

  NUMBER_OF_FILES_DELETED_STRING=$(echo "$RSYNC_OUTPUT" | grep -E "Number of deleted files: ([0-9]+)")
  NUMBER_OF_FILES_DELETED_NUMBER=$(GetFirstNumberFromString "$NUMBER_OF_FILES_DELETED_STRING")

  NUMBER_OF_FILES_TRANSFERRED_STRING=$(echo "$RSYNC_OUTPUT" | grep -E "Number of regular files transferred: ([0-9]+)")
  NUMBER_OF_FILES_TRANSFERRED_NUMBER=$(GetFirstNumberFromString "$NUMBER_OF_FILES_TRANSFERRED_STRING")

  PrintHeader "Summary of changes in: ${ENDCOLOR}$SYNC_TARGET"
  PrintLine "Number of files created: $NUMBER_OF_FILES_CREATED_NUMBER"
  PrintLine "Number of files deleted: $NUMBER_OF_FILES_DELETED_NUMBER"
  PrintLine "Number of files transferred: $NUMBER_OF_FILES_TRANSFERRED_NUMBER"

  FILES_MODIFIED=$(($NUMBER_OF_FILES_CREATED_NUMBER + $NUMBER_OF_FILES_DELETED_NUMBER + $NUMBER_OF_FILES_TRANSFERRED_NUMBER))
  if [[ "$FILES_MODIFIED" -gt 0 ]]; then
    PrintWarning "$RED_EXCLAMATION  File changes detected in $SYNC_TARGET"
    return 1
  fi

  PrintWarning "${EXCLAMATION_WHITE}  No changes detected in $SYNC_TARGET"
  return 0
}

# -------------------------------------------------------------------------------------------------------------- #
# GetFirstNumberFromString()                                                                                     #
# Parameter: $HAYSTACK The string to search                                                                      #
# Take an input string and echo the first number found.                                                          #
# This function exists primarily to parse out the output of an rsync operation and determine the number of files #
# that were created, modified, or deleted.                                                                       #
# If no number is found in the haystack string, then "0" is echoed.                                              #
# -------------------------------------------------------------------------------------------------------------- #
function GetFirstNumberFromString() {
  HAYSTACK=$1
  echo $(($(echo "$HAYSTACK" | grep -o -m 1 -E "([0-9]+)" | head -1 )))
}

# -------------------------------------------------------------------------- #
# SyncTargetDir()                                                            #
# Parameter: $SYNC_TARGET The name of a folder to sync (e.g plugins, themes) #
# Run rsync on a target directory using the default rsync arguments          #
# -------------------------------------------------------------------------- #
function SyncTargetDir() {
  SYNC_TARGET=$1
  RSYNC_ARGS=$(RsyncDefaultArgs "$SYNC_TARGET")
  PrintLine "Doing a real rsync operation"
  PrintLine "rsync $RSYNC_ARGS"
  rsync $RSYNC_ARGS > /dev/null
}

# --------------------------------------------------------- #
# PrepareGitSettings()                                      #
# Prepare the git settings in the build artifact directory. #
# --------------------------------------------------------- #
function PrepareGitSettings() {
  PrintHeader "Preparing the git settings for the build artifact directory"
  git -C "$ARTIFACT_DIR" config user.name "$USER_NAME"
  git -C "$ARTIFACT_DIR" config user.email "$USER_EMAIL"
  git -C "$ARTIFACT_DIR" config --global pager.branch false

  REMOTE_REPO_URL=$(GetRemoteRepoUrl)
  git -C "$ARTIFACT_DIR" remote add wp_engine_repo "$REMOTE_REPO_URL"
}

# ------------------------------------------------------------------------------------------------------------- #
# PrepareBuildArtifactDirectory()                                                                               #
# Create the build artifact directory.                                                                          #
# If the remote repository clone operation exits with a non-zero exit code, exit and stop the current process.  #
# ------------------------------------------------------------------------------------------------------------- #
function PrepareBuildArtifactDirectory() {
    rm -rf $ARTIFACT_DIR
    PrintHeader "Creating the build artifact directory: $ARTIFACT_DIR"
    REMOTE_REPO_URL=$(GetRemoteRepoUrl)
    git clone "$REMOTE_REPO_URL" "$ARTIFACT_DIR"
    CLONE_REPO_STATUS=$?
    if [[ "$CLONE_REPO_STATUS" -ne 0 ]]; then
      PrintError "There was an error checking out the remote repo from $REMOTE_REPO_URL"
      exit 1
    fi
}

 # ---------------------------------------------------------------------------------------------------------------- #
 # GitDeploy()                                                                                                      #
 # Add any file changes to the build artifact git repository and then push them to the WP Engine remote repository. #
 # ---------------------------------------------------------------------------------------------------------------- #
function GitDeploy() {
  git -C "$ARTIFACT_DIR" status
  git -C "$ARTIFACT_DIR" add .
  git -C "$ARTIFACT_DIR" commit -m "$CI_MESSAGE"
  git -C "$ARTIFACT_DIR" push -v -o remove-empty-dirs wp_engine_repo master
}

 # ------------------------------------------- #
 # BuildAndDeploy()                            #
 # Run the end to end build and deploy process #
 # ------------------------------------------- #
function BuildAndDeploy() {
  PreflightChecks
  PrepareBuildArtifactDirectory
  ACTUAL_SYNC_TARGETS=()

  for syncTarget in "${VALID_SYNC_TARGETS[@]}"; do
    FoundChanges "$syncTarget"
    FOUND_CHANGES_STATUS=$?
    if [[ "$FOUND_CHANGES_STATUS" -eq 1 ]]; then
        ACTUAL_SYNC_TARGETS+=("$syncTarget");
    fi
  done

  if [[ "${#ACTUAL_SYNC_TARGETS[@]}" -eq 0 ]]; then
      PrintWarning "No changes detected, skipping git deployment."
      exit 0;
  fi

  for syncTarget in "${ACTUAL_SYNC_TARGETS[@]}"; do
      PrintHeader "Syncing changes to ${syncTarget}..."
      SyncTargetDir "$syncTarget"
  done

  PrepareGitSettings
  GitDeploy
}
