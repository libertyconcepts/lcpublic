#!/usr/bin/env bash
#************************************************************************************************#
# deploy.sh                                                                                      #
#                                                                                                #
# Invoke the BuildAndDeploy function to trigger the creation and deployment of a build artifact. #
#************************************************************************************************#

if [[ "$BUILD_SCRIPTS_DIR" == "" ]]; then
  echo "The directory where the build scripts exist must be defined in an environment variable named BUILD_SCRIPTS_DIR."
  exit 1
fi

source $BUILD_SCRIPTS_DIR/build-scripts.sh


PrintEnvironmentSummary

# Invoke the build and deploy function.
BuildAndDeploy