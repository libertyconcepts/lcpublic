## About the project
The `lcpublic` project is a set of shell scripts created to facilitate the deployment of WordPress projects to 
WP Engine.  The `deploy.sh` script is intended for use in a [Codeship CI/CD](https://www.cloudbees.com/products/codeship)
environment, and relies on vendor specific environment variables.

## Getting started

```
# Cloning a fresh copy of the project
git clone git@bitbucket.org:libertyconcepts/lcpublic.git --recurse-submodules
```

If you have previously cloned a copy of the repo but do not have the `bats-core` git submodules, you can run the
following command:
```
git submodule update --init --recursive
```

### Running the scripts in Codeship
To run the build scripts in Codeship, add the following code to the Codeship project's deploy steps 
```
BUILD_SCRIPTS_DIR="~/src/commands" bash ~/src/commands/deploy.sh
```
### Running tests locally
```
BUILD_SCRIPTS_DIR="</absolute/path/to/>lcpublic" test/bats/bin/bats test/build-scripts.bats
```
