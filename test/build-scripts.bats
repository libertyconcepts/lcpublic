setup() {
    load 'test_helper/bats-support/load'
    load 'test_helper/bats-assert/load'

    export CLONE_DIR="$BATS_TMPDIR/clone-dir"
    load "$BUILD_SCRIPTS_DIR/build-scripts.sh"
}

teardown() {
    rm -rf "$BATS_TEST_TMPDIR"
}

@test "Verify the results of GetEnvFilePath()" {
    run GetEnvFilePath
    [ "$status" -eq 0 ]
    [ "$output" = "$CLONE_DIR/.env" ]
}

@test "Test ValidateDotEnvFileExists() with a valid .env file path" {
    export CLONE_DIR="$BATS_TEST_DIRNAME/data"
    run ValidateDotEnvFileExists
    [ "$status" -eq 0 ]
}

@test "Test ValidateDotEnvFileExists() with an  invalid .env file path" {
    export CLONE_DIR="$BATS_TEST_DIRNAME/data/invalid"
    run ValidateDotEnvFileExists
    [ "$status" -eq 1 ]
}

@test "Test ValidateEnvironmentVars() with a valid environment" {
    export CLONE_DIR="A"
    export BRANCH="B"
    export USER_EMAIL="C"
    export USER_NAME="D"

    run ValidateEnvironmentVars
    [ "$status" -eq 0 ]
}

@test "Test ValidateEnvironmentVars() with an invalid CLONE_DIR environment variable" {
    export CLONE_DIR=""
    export BRANCH="B"
    export USER_EMAIL="C"
    export USER_NAME="D"
    run ValidateEnvironmentVars
    [ "$status" -eq 1 ]
}

@test "Test ValidateEnvironmentVars() with an invalid BRANCH environment variable" {
    export CLONE_DIR="A"
    export BRANCH=""
    export USER_EMAIL="C"
    export USER_NAME="D"
    run ValidateEnvironmentVars
    [ "$status" -eq 1 ]
}

@test "Test ValidateEnvironmentVars() with an invalid USER_EMAIL environment variable" {
    export CLONE_DIR="A"
    export BRANCH="B"
    export USER_EMAIL=""
    export USER_NAME="D"
    run ValidateEnvironmentVars
    [ "$status" -eq 1 ]
}

@test "Test ValidateEnvironmentVars() with an invalid USER_NAME environment variable" {
    export CLONE_DIR="A"
    export BRANCH="B"
    export USER_EMAIL="C"
    export USER_NAME=""
    run ValidateEnvironmentVars
    [ "$status" -eq 1 ]
}

@test "Test ValidateDeployBranch() with valid deploy branches" {
    for validBranch in "${VALID_BRANCHES[@]}"; do
      export BRANCH=$validBranch
      echo "# => Testing ValidateDeployBranch() with a branch of $BRANCH" >&3
      run ValidateDeployBranch
      [ "$status" -eq 0 ]
    done
}

@test "Test ValidateDeployBranch() with an invalid deploy branch" {
    export BRANCH="invalid_branch_name"
    run ValidateDeployBranch
    [ "$status" -eq 1 ]
}

@test "Test GetRemoteRepoUrl() with well configured .env file variables" {
    export BRANCH="dev"
    export DEV_REPO="git@git.wpengine.com:production/test-repo-url.git"
    run GetRemoteRepoUrl
    [ "$output" = "$DEV_REPO" ]
    [ "$status" -eq 0 ]
}


@test "Test GetRemoteRepoUrl() with an invalid deploy branch" {
    export BRANCH="invalid_deploy_branch"
    export DEV_REPO="git@git.wpengine.com:production/test-repo-url.git"
    run GetRemoteRepoUrl
    [ "$status" -eq 1 ]
}

@test "Test GetRemoteRepoUrl() with an invalid repo URL" {
    export BRANCH="invalid_deploy_branch"
    export DEV_REPO=""
    run GetRemoteRepoUrl
    [ "$status" -eq 1 ]
}

@test "Test IsValidTargetType() with valid targets" {
    for target in "${VALID_SYNC_TARGETS[@]}"; do
      echo "# => Testing IsValidTargetType() with a target of $target" >&3
      run IsValidTargetType "$target"
      [ "$status" -eq 0 ]
    done
}

@test "Test IsValidTargetType() with an invalid target" {
    run IsValidTargetType "invalid-target-type"
    [ "$status" -eq 1 ]
}

@test "Test PreflightChecks() with a valid environment" {
    export BRANCH="production"
    export CLONE_DIR="$BATS_TEST_DIRNAME/data"
    export USER_NAME="Test user name"
    export USER_EMAIL="test@example.com"
    run PreflightChecks
    [ "$status" -eq 0 ]
}

@test "Test PreflightChecks() with an invalid deploy branch" {
    export BRANCH="invalid-deploy-branch"
    export CLONE_DIR="$BATS_TEST_DIRNAME/data"
    export USER_NAME="Test user name"
    export USER_EMAIL="test@example.com"
    run PreflightChecks
    [ "$status" -eq 1 ]
}

@test "Test PreflightChecks() with a missing .env file" {
    export BRANCH="production"
    export CLONE_DIR="$BATS_TEST_DIRNAME/data/invalid-path"
    export USER_NAME="Test user name"
    export USER_EMAIL="test@example.com"
    run PreflightChecks
    [ "$status" -eq 1 ]
}

@test "Test PreflightChecks() with a missing USER_NAME environment variable" {
    export BRANCH="production"
    export CLONE_DIR="$BATS_TEST_DIRNAME/data"
    export USER_NAME=""
    export USER_EMAIL="test@example.com"
    run PreflightChecks
    [ "$status" -eq 1 ]
}

@test "Test PreflightChecks() with a missing USER_EMAIL environment variable" {
    export BRANCH="production"
    export CLONE_DIR="$BATS_TEST_DIRNAME/data"
    export USER_NAME="Test user name"
    export USER_EMAIL=""
    run PreflightChecks
    [ "$status" -eq 1 ]
}

@test "Test RsyncDefaultArgs() with a valid target" {
    run RsyncDefaultArgs "valid-sync-target"
    [ "$status" -eq 0 ]
}

@test "Test RsyncDefaultArgs() with an invalid target" {
    run RsyncDefaultArgs ""
    [ "$status" -eq 1 ]
}

@test "Test EnsureTargetDirectoryExists() with a directory that exists" {
    TARGET_DIR="$BATS_TEST_TMPDIR/extant-directory"
    mkdir "$TARGET_DIR"
    run EnsureTargetDirectoryExists "$TARGET_DIR"
    [ "$status" -eq 0 ]
}

@test "Test EnsureTargetDirectoryExists() with a directory that does not exist" {
    TARGET_DIR="$BATS_TEST_TMPDIR/non-extant-directory"
    run EnsureTargetDirectoryExists "$TARGET_DIR"
    [ "$status" -eq 1 ]
}

@test "Test FoundChanges() with changes present" {
    ARTIFACT_DIR="$BATS_TEST_TMPDIR/build-artifact"
    CLONE_DIR="$BATS_TEST_TMPDIR/clone-dir"

    TARGET="plugins"
    PLUGIN_FOLDER="test-plugin-folder"
    SOURCE_REPO_TARGET="$CLONE_DIR/wp-content/$TARGET/$PLUGIN_FOLDER"
    BUILD_ARTIFACT_TARGET="$ARTIFACT_DIR/wp-content/$TARGET/$PLUGIN_FOLDER"

    # Create test directories and files
    mkdir -p "$SOURCE_REPO_TARGET"
    mkdir -p "$BUILD_ARTIFACT_TARGET"
    echo "A" > "$SOURCE_REPO_TARGET/test-file.txt"
    echo "B" > "$SOURCE_REPO_TARGET/test-file.txt"

    run FoundChanges "$TARGET"

    # Clean up temporary directories.
    rm -rf $ARTIFACT_DIR
    rm -rf $CLONE_DIR

    [ "$status" -eq 1 ]
}

@test "Test FoundChanges() with no changes present" {
    ARTIFACT_DIR="$BATS_TEST_TMPDIR/build-artifact"
    CLONE_DIR="$BATS_TEST_TMPDIR/clone-dir"

    TARGET="plugins"
    PLUGIN_FOLDER="test-plugin-folder"
    SOURCE_REPO_TARGET="$CLONE_DIR/wp-content/$TARGET/$PLUGIN_FOLDER"
    BUILD_ARTIFACT_TARGET="$ARTIFACT_DIR/wp-content/$TARGET/$PLUGIN_FOLDER"
    TEST_FILE_NAME="test-file.txt"

    # Create test directories and files
    mkdir -p "$SOURCE_REPO_TARGET"
    mkdir -p "$BUILD_ARTIFACT_TARGET"

    echo "A" > "$SOURCE_REPO_TARGET/$TEST_FILE_NAME"
    cp "$SOURCE_REPO_TARGET/$TEST_FILE_NAME" "$BUILD_ARTIFACT_TARGET/$TEST_FILE_NAME"

    run FoundChanges "$TARGET"

    # Clean up temporary directories.
    rm -rf $ARTIFACT_DIR
    rm -rf $CLONE_DIR

    [ "$status" -eq 0 ]
}

@test "Test FoundChanges() with invalid rsync target directories" {
    TARGET="plugins"
    run FoundChanges "$TARGET"
    [ "$status" -eq 1 ]
}

@test "Test GetFirstNumberFromString() with a valid number" {
    run GetFirstNumberFromString "Testing string with 7 and then 5"
    [ "$output" = "7" ]
}

@test "Test GetFirstNumberFromString() with no valid number" {
    run GetFirstNumberFromString "Testing string with no number"
    [ "$output" = "0" ]
}

@test "Test SyncTargetDir()" {
    ARTIFACT_DIR="$BATS_TEST_TMPDIR/build-artifact"
    CLONE_DIR="$BATS_TEST_TMPDIR/clone-dir"
    TARGET="plugins"

    SOURCE_REPO_TARGET="$CLONE_DIR/wp-content/$TARGET/$PLUGIN_FOLDER"
    BUILD_ARTIFACT_TARGET="$ARTIFACT_DIR/wp-content/$TARGET/$PLUGIN_FOLDER"

    mkdir -p "$SOURCE_REPO_TARGET"
    mkdir -p "$BUILD_ARTIFACT_TARGET"

    run FoundChanges "$TARGET"
    [ "$status" = 0 ]

    rm -rf $BATS_TEST_TMPDIR
}

@test "Test PrepareGitSettings()" {
    BRANCH="production"
    PRODUCTION_REPO="git@git.wpengine.com:production/production-repo.git"
    USER_NAME="Test user"
    USER_EMAIL="test@example.com"

    ARTIFACT_DIR="$BATS_TEST_TMPDIR/build-artifact"
    mkdir "$ARTIFACT_DIR"
    git -C "$ARTIFACT_DIR" init

    run PrepareGitSettings
    [ "$status" = 0 ]
}
