#!/usr/bin/env bash

# https://emojipedia.org/
# https://onlineutf8tools.com/convert-utf8-to-hexadecimal

GREEN_COLOR_CODE="32"
RED_COLOR_CODE="31"
YELLOW_COLOR_CODE="33"
GREEN="\e[${GREEN_COLOR_CODE}m"
RED="\e[${RED_COLOR_CODE}m"
YELLOW="\e[${YELLOW_COLOR_CODE}m"
BOLDGREEN="\e[1;${GREEN_COLOR_CODE}m"
BOLDRED="\e[1;${RED_COLOR_CODE}m"
BOLDYELLOW="\e[1;${YELLOW_COLOR_CODE}m"
ENDCOLOR="\e[0m"
EXCLAMATION_WHITE="\xe2\x9d\x95"
CHECKMARK="\xE2\x9C\x94"
XMARK="\xE2\x9D\x8C"
BULLET="\xE2\x80\xA2"
CHECK_MARK_BUTTON="\xe2\x9c\x85"
RED_EXCLAMATION="\xe2\x9d\x97"
STOP_SIGN="\xf0\x9f\x9b\x91"
INFO_ICON="\xe2\x84\xb9\xef\xb8\x8f"

function PrintLine() {
  printf "$1$ENDCOLOR\n"
}

function PrintError() {
  PrintLine "$STOP_SIGN ${RED} $1"
}

function PrintSuccess() {
  PrintLine "$CHECK_MARK_BUTTON  $1"
}

function PrintWarning() {
  PrintLine "${YELLOW}$1"
}

function PrintInfo() {
  PrintLine "$INFO_ICON $1"
}

function PrintHeader() {
  TEXT="$1"
  SEPARATOR="[==============================================]"
  printf "\n${BOLDGREEN}$SEPARATOR\n"
  printf "${BOLDGREEN}| $TEXT\n"
  printf "${BOLDGREEN}$SEPARATOR\n"
  printf "$ENDCOLOR"
}


function DirectoryExists() {
	if [[ ! -d "$1" ]]; then
	  return 1
	fi

	return 0;
}

function FileExists() {
	if [[ ! -f "$1" ]]; then
	  return 1
	fi

	return 0;
}

function ToUpperCase() {
  echo "$(echo "$1" | tr '[:lower:]' '[:upper:]')"
}
